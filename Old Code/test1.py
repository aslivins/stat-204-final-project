# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import time
import re
tic = time.time()
X = pd.read_csv('data/test1.txt', sep="\t", header=None)
print(X)

Y = X[6]
#count = 0

#Vector represents what variables are being accounted for
#vars = [fga, fgm, 3pa, 3pm, site, team, assists, blocks, DReb, OReb, fouls,
        # fta, ftm, margin, turnovers, steals, points]
VarNames = ['Field Goals Attempted', 'Field Goals Made', 'Three Pointers Attempted', 'Three Pointers, Made', 'Home', 'Team', 'Assists', 'Blocks', 'Defensive Rebounds', 'Offensive Rebounds', 'Fouls', 'Free Throws Attempted', 'Free Throws Made', 'Margin', 'Turnovers', 'Steals', 'Points']
#print(len(VarNames))
varInd = [1, 3, 5, 7, 12, 14, 16, 18, 20, 22, 24, 28, 32, 34, 36]
rows, cols = (30, 17)
vars = [[0 for i in range(cols)] for j in range(rows)]
#print(len(vars))
#print(Y)
#print(len(Y))
index1 = 0
for i in Y:
    if (i == 'query'):
        continue
    split_string = re.split(r'= | and',i)
    #print(split_string)
    #print(len(split_string))
    #if (len(split_string) != 37):
        #count = count + 1
    index2 = 0
    for j in varInd:
        if (index2 == 4):
            index2 += 2
        vars[index1][index2] = int(split_string[j])
        index2 +=1
    
    vars[index1][4] = split_string[9]
    vars[index1][5] = split_string[10]
        
    index1 += 1
        
print(vars)
print(len(vars))

Z = pd.DataFrame(vars, columns=VarNames)
print(Z)
Z.to_csv('data/cleanedGame12021.csv')
        
#print(count)
toc = time.time()

print(toc-tic, 'sec elapsed')