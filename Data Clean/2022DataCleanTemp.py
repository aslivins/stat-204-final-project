# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import time
import re
tic = time.time()
X = pd.read_csv('data/Data2022Temp.txt', sep="\t", header=None)
print(X)

gameNum = 19

Y = X[6]
#count = 0

#Vector represents what variables are being accounted for
VarNames = ['Game Number', 'Average Line', 'Team', 'Home', 'Points', 'Margin', 'Field Goals Attempted', 'Field Goals Made', 'Three Pointers Attempted', 'Three Pointers Made', 'Assists', 'Blocks', 'steals', 'turnovers', 'Defensive Rebounds', 'Offensive Rebounds', 'Fouls', 'Free Throws Attempted', 'Free Throws Made', 'Opp Team', 'Opp Points', 'Opp Field Goals Attempted', 'Opp Field Goals Made', 'Opp Three Pointers Attempted', 'Opp Three Pointers Made', 'Opp Assists', 'Opp Blocks', 'Opp Steals', 'Opp Turnovers', 'Opp Defensive Rebounds', 'Opp Offensive Rebounds', 'Opp Fouls', 'Opp Free Throws Attempted', 'Opp Free Throws Made']
print(len(VarNames))

varInd = [4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61]
print(len(varInd))
rows, cols = (28, 34)
vars = [[0 for i in range(cols)] for j in range(rows)]
#print(len(vars))
#print(Y)
#print(len(Y))
index1 = 0
for i in Y:
    if (i == 'query'):
        continue
    split_string = re.split(r'= | and',i)
    print(split_string)
    #print(len(split_string))
    #if (len(split_string) != 37):
        #count = count + 1
    
    index2 = 4
    vars[index1][0] = gameNum
    vars[index1][1] = X[2][index1 + 1]
    
    vars[index1][2] = split_string[0]
    if (split_string[2] == 'home'):
        vars[index1][3] = 1
    if (split_string[2] != 'home'):
        vars[index1][3] = 0
    for j in varInd:
        if (index2 == 19):
            temp = split_string[j]
            new_split = re.split(r":",temp)
            vars[index1][index2] = new_split[1]
            index2 += 1
            continue
        vars[index1][index2] = int(split_string[j])
        index2 +=1        
    index1 += 1
        
print(vars)
print(len(vars))

Z = pd.DataFrame(vars, columns=VarNames)
print(Z)
Z.to_csv('data/2022CleanedDataTemp.csv')
        
#print(count)
toc = time.time()

print(toc-tic, 'sec elapsed')