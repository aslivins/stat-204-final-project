# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import time
import re
import numpy
import string
tic = time.time()
X = pd.read_csv('data/moneyLines.txt', sep="\t", header=None)
#print(X)
#print(len(X))
#print(X[0][4])
Y = [0]

for i in range(2460):
    a = X[0][i*3]
    Y = numpy.vstack([Y,a])
    
    
#print(Y[5])
Z = [0, 1, 2, 3]
#Vector represents what variables are being accounted for
VarNames = ['Game Number', 'Team', 'Site', 'moneyLine']
#print(len(VarNames))
    
rows, cols = (2460, 4)
vars = [[0 for i in range(cols)] for j in range(rows)]
for i in Y:
    #print(i[0])
    #print(type(i[0]))
    temp = str(i[0])
    #print(temp)
    gameNum = temp[94:96]
    #print(len(gameNum))
    if (len(gameNum) >= 2):
        #print(gameNum)
        site = temp[198:202]
        #print(site)
        moneyLine = temp[248:253]
        #print(moneyLine)
        team = temp[333:337]
        #print(team)
    if (len(gameNum) < 2):
        #print(gameNum)
        site = temp[195:199]
        #print(site)
        moneyLine = temp[245:250]
        #print(moneyLine)
        team = temp[330:334]
        #print(team)

    VarNames = numpy.vstack([VarNames, [gameNum, team, site, moneyLine]])
    
print(VarNames)
#print(Y[2][0])
    
"""
#Vector represents what variables are being accounted for
VarNames = ['Game Number', 'Average Line', 'Team', 'Home', 'Points', 'Margin', 'Field Goals Attempted', 'Field Goals Made', 'Three Pointers Attempted', 'Three Pointers Made', 'Assists', 'Blocks', 'steals', 'turnovers', 'Defensive Rebounds', 'Offensive Rebounds', 'Fouls', 'Free Throws Attempted', 'Free Throws Made', 'Opp Team', 'Opp Points', 'Opp Field Goals Attempted', 'Opp Field Goals Made', 'Opp Three Pointers Attempted', 'Opp Three Pointers Made', 'Opp Assists', 'Opp Blocks', 'Opp Steals', 'Opp Turnovers', 'Opp Defensive Rebounds', 'Opp Offensive Rebounds', 'Opp Fouls', 'Opp Free Throws Attempted', 'Opp Free Throws Made']
print(len(VarNames))

varInd = [4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61]
print(len(varInd))
rows, cols = (540, 34)
vars = [[0 for i in range(cols)] for j in range(rows)]
#print(len(vars))
#print(Y)
#print(len(Y))
index1 = 0
for i in Y:
    if (i == 'query'):
        continue
    split_string = re.split(r'= | and',i)
    print(split_string)
    #print(len(split_string))
    #if (len(split_string) != 37):
        #count = count + 1
    
    index2 = 4
    vars[index1][0] = int(index1/30) + 1
    vars[index1][1] = X[2][index1 + 1]
    
    vars[index1][2] = split_string[0]
    if (split_string[2] == 'home'):
        vars[index1][3] = 1
    if (split_string[2] != 'home'):
        vars[index1][3] = 0
    for j in varInd:
        if (index2 == 19):
            temp = split_string[j]
            new_split = re.split(r":",temp)
            vars[index1][index2] = new_split[1]
            index2 += 1
            continue
        vars[index1][index2] = int(split_string[j])
        index2 +=1        
    index1 += 1
        
print(vars)
print(len(vars))

Z = pd.DataFrame(vars, columns=VarNames)
print(Z)
Z.to_csv('data/2022CleanedData.csv')
        
#print(count)
toc = time.time()

print(toc-tic, 'sec elapsed')
"""
