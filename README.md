# Machine Learning Model to Predict the Outcomes of NBA Games



## Project Goals

1) Use SQL to gather data from KillerSports.com
2) Use Python to clean and format the queries into a data table
3) Use R to fit a logistic model to the binary Win/Loss variable using best subset feature selection
4) Use the model to predict daily NBA game outcomes
5) Evaluate model's performance over time, adjust and repeat

Deliverable: NBA Win Predictions

Spreadsheet with predictions for November 2022(Excluding the first 5 games to get decent average team stats):
https://docs.google.com/spreadsheets/d/12TbLb_Rsriv0Som8Yh7m5zJPrxs0lw4Lu9xX42YSoBk/edit?usp=sharing
